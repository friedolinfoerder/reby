# encoding: UTF-8
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

require_relative 'core/reby'
Reby
  .initializeDatabase("database.yml")
  .debug!