# encoding: UTF-8

class HomeResolver < ViewResolver

  get %r{^/?$}i do
    resolve("home", "welcome", params)
  end



end