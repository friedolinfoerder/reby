# encoding: UTF-8

class HomePresenter < Presenter

  def welcome params
    page[:title] = "Welcome to Reby!"
  end

end